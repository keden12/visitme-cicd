/*eslint no-unused-vars: "off" */
let express = require("express")
let path = require("path")
let favicon = require("serve-favicon")
let logger = require("morgan")
let cookieParser = require("cookie-parser")
let bodyParser = require("body-parser")
const cors = require("cors")
var indexRouter = require("./routes/index")
var usersRouter = require("./routes/users")

const listings = require("./routes/listings")

let app = express()

// view engine setup
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, "public")))
if (process.env.NODE_ENV !== "test") {  
  app.use(logger("dev"))
}
app.use("/", indexRouter)
app.use("/users", usersRouter)

app.get("/listings", listings.findListings)

app.get("/listings/total", listings.findTotalListings)
app.get("/listings/:category/total", listings.findTotalListingsByCategory)

app.get("/listings/:id", listings.findByID)
app.get("/listings/title/:title", listings.findByTitle)
app.get("/listings/category/:category", listings.findByCategory)


app.put("/listings/:id/love", listings.incrementHeart)

app.put("/listings/:id/changetitle", listings.changeTitle)
app.put("/listings/:id/changedesc", listings.changeDescription)
app.put("/listings/:id/changefeatured", listings.changeFeatured)

app.post("/listings",listings.addListing)
app.post("/listings/search", listings.searchListings)

app.delete("/listings/:id", listings.deleteListingByID)
app.delete("/listings/title/:title", listings.deleteListingByTitle)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error("Not Found")
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500)
    res.render("error", {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500)
  res.render("error", {
    message: err.message,
    error: {}
  })
})

module.exports = app
